package com.raven.lab1;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity
{
    LayoutInflater layoutInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutInflater = getLayoutInflater();

        GridView gridView = findViewById(R.id.gridView);
        final int lastVisiblePosition = gridView.getFirstVisiblePosition();
        gridView.setAdapter(new BaseAdapter()
        {
            @Override
            public int getCount()
            {
                return 24;
            }

            @Override
            public Object getItem(int i)
            {
                return null;
            }

            @Override
            public long getItemId(int i)
            {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup)
            {
                return layoutInflater.inflate(R.layout.cell, viewGroup, false);
            }
        });
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            boolean fling;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                fling = scrollState == SCROLL_STATE_FLING;
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (fling)
                {
                    fling = false;
                    absListView.setSelection(firstVisibleItem);
                }
            }
        });
    }
}